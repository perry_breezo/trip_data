package com.perrchick.firstflutterapp

import android.os.Bundle

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
//import org.junit.runner.Request.method

class MainActivity: FlutterActivity() {
    private val CHANNEL_NAME = "main.breezometer/native_channel"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        MethodChannel(flutterView, CHANNEL_NAME).setMethodCallHandler { methodCall, methodResult ->
            print(methodCall.arguments)

            when (methodCall.method) {
                "open_maps" -> {
                    methodResult.success("1")
                } else -> {
                    methodResult.success("0")//methodResult.notImplemented()
                }
            }
        }
    }
}