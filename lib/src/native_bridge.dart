import 'dart:async';
import 'package:flutter/services.dart';

// Inspired from: https://flutter.io/docs/development/platform-integration/platform-channels
class NativeRunner {
  static const platform =
      const MethodChannel('main.breezometer/native_channel');

  static Future<String> openMaps() async {
    String result;
    try {
      result = await platform.invokeMethod('open_maps');
      print('Native method returned result: $result');
    } on PlatformException catch (e) {
      print("Failed to run native method, error: '${e.message}'.");
    }

    return result;
  }
}
