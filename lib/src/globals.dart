import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';

double currentLatitude;
double currentLongitude;

class Utils {
  // From: https://pub.dartlang.org/packages/fluttertoast
  static void toast(String toastMessage) {
    Fluttertoast.showToast(
        msg: toastMessage,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 3,
        backgroundColor: Colors.red,
        textColor: Colors.white);
  }
}
