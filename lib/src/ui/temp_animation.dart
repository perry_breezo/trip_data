import 'package:flutter/material.dart';
import "package:flare_flutter/flare_actor.dart";

class AnimationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
        child: SizedBox(
            width: 200,
            height: 200,
            child: new FlareActor(
                "lib/src/resources/animations/boring_star.flr",
                alignment: Alignment.center,
                fit: BoxFit.contain,
                animation: "rotate_scale_color")));
    ;
  }
}
