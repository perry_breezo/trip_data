import 'package:flutter/material.dart';
import 'home_page.dart';
import 'ui/movie_list.dart';
import 'ui/tabs.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          body: MyHomePage(title: 'Breezo Demo'),
          //body: MovieList(),
          //body: Tabs()
        ) //,
        );
  }
}
