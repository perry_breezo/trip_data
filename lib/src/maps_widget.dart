import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import './globals.dart' as globals;

// Add Google Maps: https://medium.com/flutter-community/flutter-how-to-integrate-google-maps-experimental-5ec8485c0c59
// More about Google Maps: https://medium.com/flutter-community/exploring-google-maps-in-flutter-8a86d3783d24

// Taken from: https://pub.dartlang.org/documentation/google_maps_flutter/latest/google_maps_flutter/MapType-class.html
class MapsWidget extends StatefulWidget {
  final VoidCallback onMapReady;
  MapsWidget({@required this.onMapReady});

  final MapsWidgetState mapsWidgetState = MapsWidgetState();

  void updateCamera() {
    mapsWidgetState.updateCamera();
  }

  void addBaqiMarker(double latitude, double longitude, int baqi) {
    mapsWidgetState.addBaqiMarker(latitude, longitude, baqi);
  }

  @override
  State createState() => mapsWidgetState;
}

class MapsWidgetState extends State<MapsWidget> {
  GoogleMapController mapController;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Center(
            child: SizedBox(
              width: double.infinity,
              height: 300.0,
              child: GoogleMap(
                options: GoogleMapOptions(
                    scrollGesturesEnabled: false,
                    //myLocationEnabled: true,
                    trackCameraPosition: true,
                    zoomGesturesEnabled: true,
                    compassEnabled: true),
                onMapCreated: _onMapCreated,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
    //onMapReady();
    // controller.addListener(() {
    //   print("map listener invoked");
    // });
    widget.onMapReady();
  }

  void addBaqiMarker(double latitude, double longitude, int baqi) {
    // mapView.setPolylines(<Polyline>[
    //     new Polyline(
    //       "11",
    //       <Location>[
    //         new Location(45.523970, -122.663081),
    //         new Location(45.528788, -122.684633),
    //         new Location(45.528864, -122.667195),
    //       ],
    //       jointType: FigureJointType.round,
    //       width: 15.0,
    //       color: Colors.orangeAccent,
    //     ),
    //     new Polyline(
    //       "12",
    //       <Location>[
    //         new Location(45.519698, -122.674932),
    //         new Location(45.516687, -122.667014),
    //       ],
    //       width: 15.0,
    //     ),
    //   ]);

    mapController.addMarker(
      MarkerOptions(
        position: LatLng(latitude, longitude),
        infoWindowText: InfoWindowText("BAQI", "$baqi"),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      ),
    );
  }

  void updateCamera() {
    if (mapController == null ||
        globals.currentLatitude == null ||
        globals.currentLatitude == null ||
        globals.currentLatitude == 0 ||
        globals.currentLongitude == 0) return;
    mapController.animateCamera(CameraUpdate.newCameraPosition(
      new CameraPosition(
        bearing: 0.0,
        target: LatLng(globals.currentLatitude, globals.currentLongitude),
        tilt: 0.0,
        zoom: 18.0,
      ),
    ));
  }
}
