import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'package:latlong/latlong.dart';
import 'ui/temp_animation.dart';

//import './maps_widget.dart';
import './login_screen.dart';
import './globals.dart' as globals;
import './native_bridge.dart' as nativeBridge;
import './ui/tabs.dart';
import './ui/movies.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // These keys where taken from our existing Android app:
  final String APP_KEY = "68fccdf466ac4aa6b8c467ea93870d9f";
  final double MINIMUM_DISTANCE_TO_UPDATE = 300.0;

  List<http.Response> responses = []; //http.Response
  var lastFetchedData = -1;

  Map<String, double> _startLocation;
  Map<String, double> _currentLocation;
  String _currentLocationString;
  String _currentPollenData;
  //MapsWidget mapsWidget;
  bool isMapsReady = false;

  StreamSubscription<Map<String, double>> _locationSubscription;
  // Inspired form: https://pub.dartlang.org/packages/location
  Location locationManager = new Location();
  final Distance distance = new Distance();

  bool isLocationPermissionGranted = false;
  String error;

  bool currentWidget = true;

  Image image1;

  int _counter = 0;

  bool isRecording = false;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  void onMapReady() async {
    isMapsReady = true;

    if (globals.currentLongitude != null) {
      //mapsWidget.updateCamera();
    }
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();

    _locationSubscription = locationManager
        .onLocationChanged()
        .listen((Map<String, double> result) {
      setState(() {
        _currentLocation = result;
        double accuracy = result["accuracy"];
        _currentLocationString =
            '(${globals.currentLongitude},\n${globals.currentLatitude})\nlocation accuracy = $accuracy';
        final updatedLongitude = result["longitude"];
        final updatedLatitude = result["latitude"];

        double distanceInMeters = MINIMUM_DISTANCE_TO_UPDATE;
        if (globals.currentLatitude != null &&
            globals.currentLongitude != null) {
          distanceInMeters = distance(
              new LatLng(globals.currentLatitude, globals.currentLongitude),
              new LatLng(updatedLatitude, updatedLongitude));
        }

        //print("distanceInMeters: $distanceInMeters");
        //if (distanceInMeters < MINIMUM_DISTANCE_TO_UPDATE) return;

        globals.currentLatitude = updatedLatitude;
        globals.currentLongitude = updatedLongitude;

        if (!isRecording) return;

        //fetchData();

        if (isMapsReady) {
          //mapsWidget.updateCamera();
        }
      });
    });

    //mapsWidget = MapsWidget(onMapReady: this.onMapReady);
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  initPlatformState() async {
    Map<String, double> location;
    // Platform messages may fail, so we use a try/catch PlatformException.

    try {
      isLocationPermissionGranted = await locationManager.hasPermission();
      location = await locationManager.getLocation();

      error = null;
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error =
            'Permission denied - please ask the user to enable it from the app settings';
      }

      location = null;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    //if (!mounted) return;

    setState(() {
      _startLocation = location;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets;

    var tabsButton = Card(
      child: FlatButton(
        child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 200),
            child: SizedBox.expand(
              child:
                  const Text('Open Tabs Screen', textAlign: TextAlign.center),
            )),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => TabsScreen()),
          );
        },
      ),
    );

    if (_currentLocation == null) {
      widgets = new List();
    } else {
      widgets = [
        new Image.network(
            "https://maps.googleapis.com/maps/api/staticmap?center=${_currentLocation["latitude"]},${_currentLocation["longitude"]}&zoom=18&size=640x400&key=AIzaSyChgTTfM3JsXfmIqjcyUP1g-UVUTqVEZBk")
      ];
    }

    widgets.add(new Center(
        child: new Text(_startLocation != null
            ? 'Start location: $_startLocation\n'
            : 'Error: $error\n')));

    widgets.add(new Center(
        child: new Text(_currentLocation != null
            ? 'Continuous location: $_currentLocation\n'
            : 'Error: $error\n')));

    widgets.add(new Center(
        child: new Text(isLocationPermissionGranted
            ? 'Has location permission : Yes'
            : "Has location permission : No")));

    // return new MaterialApp(
    //     home: new Scaffold(
    //         appBar: new AppBar(
    //           title: Text(widget.title),
    //         ),
    //         body: new Column(
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           mainAxisSize: MainAxisSize.min,
    //           children: widgets,
    //         )));

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            // Column is also layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Text(
              //   'You pressed the button $_counter times',
              // ),
              Text(
                'Location:',
                style: Theme.of(context).textTheme.display1,
              ),
              Text(
                '$_currentLocationString',
              ),
              Text(
                '\nInfo:',
                style: Theme.of(context).textTheme.display1,
              ),
              Text(
                '$_currentPollenData',
              ),
              //mapsWidget,
              RaisedButton(
                child: new Text(
                    isRecording ? 'Stop recording' : 'Start recording'),
                color: isRecording ? Colors.red : Colors.green,
                onPressed: () {
                  toggleRecord();
                  setState(() {});
                },
              ),
              RaisedButton(
                child: new Text('open native map'),
                color: Colors.grey,
                onPressed: () {
                  nativeBridge.NativeRunner.openMaps()
                      .then((nativeRunnerResult) {
                    //var nativeRunnerResult =;
                    int nativeRunnerResultInt =
                        int.tryParse(nativeRunnerResult) ?? 0;
                    if (nativeRunnerResultInt == 1) {
                      // Succeeded!
                      _locationSubscription.cancel();
                    }
                  });
                },
              ),
              RaisedButton(
                child: new Text('open next screen'),
                color: Colors.grey,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SecondScreen()),
                  );
                },
              ),

              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Center(
                    // Center is a layout widget. It takes a single child and positions it
                    // in the middle of the parent.
                    child: new Container(
                  height: 400,
                  padding: new EdgeInsets.all(32.0),
                  child: Center(
                    child: new Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Card(
                          child: new Container(
                            padding: new EdgeInsets.all(32.0),
                            child: new Column(
                              children: <Widget>[
                                //TODO: Embed this: https://github.com/xqwzts/flutter_sparkline
                                new AnimationWidget(),
                              ],
                            ),
                          ),
                        ),
                        new Card(
                          child: new Container(
                            padding: new EdgeInsets.all(32.0),
                            child: new Column(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                FlatButton(
                                  child: const Text('Open Movies Screen'),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => MoviesScreen()),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        tabsButton
                      ],
                    ),
                  ),
                )),
              )
            ],
          ),
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: mapsWidget.updateCamera,
      //   tooltip: 'tap to increment',
      //   child: Icon(Icons.location_on),
      // ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  void fetchData() async {
    var now = new DateTime.now().millisecondsSinceEpoch;
    if (now - lastFetchedData < 5000) return; // Throttle
    lastFetchedData = now;

    if (globals.currentLatitude == null || globals.currentLongitude == null)
      return;
    if (globals.currentLatitude == 0 || globals.currentLongitude == 0) return;

    final latitude = globals.currentLatitude;
    final longitude = globals.currentLongitude;
    String urlString =
        'https://api.breezometer.com/air-quality/v2/current-conditions?lat=${globals.currentLatitude}&lon=${globals.currentLongitude}&key=$APP_KEY&features=breezometer_aqi,local_aqi,health_recommendations,sources_and_effects,pollutants_concentrations,pollutants_aqi_information';
    var client = new http.Client();
    http.Response response = await client.get(urlString);
    //.then((response) {
    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");
    if (response.statusCode == 200) {
      responses.add(response);
      Map mappedResponse = jsonDecode(response.body);
      Map mappedData = mappedResponse['data'];
      Map mappedIndexes = mappedData['indexes'];
      Map mappedBaqi = mappedIndexes['baqi'];

      _currentPollenData = mappedBaqi['aqi_display']; //'$baqi';
      int baqi = mappedBaqi['aqi']; //int.tryParse(_currentPollenData) ?? -1;
      //mapsWidget.addBaqiMarker(latitude, longitude, baqi);
    }
    //});
  }

  void toggleRecord() {
    isRecording = !isRecording;

    if (!isRecording) {
      _showDialog("Stop recording?", "will navigate to results screen");
    }
  }

  // From: https://medium.com/@nils.backe/flutter-alert-dialogs-9b0bb9b01d28
  void _showDialog(String title, String body) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(body),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                globals.Utils.toast("Closing...");
                //Navigator.of(context, rootNavigator: true).pop('dialog');
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
