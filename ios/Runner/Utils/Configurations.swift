//
//  Configurations.swift
//  SomeApp
//
//  Created by Perry on 2/13/16.
//  Copyright © 2016 PerrchicK. All rights reserved.
//

import Foundation
//import FirebaseRemoteConfig

class Configurations {
    static let shared = Configurations()

    struct Constants {
        static let ClosestZoomRatioScale: Double = 591657550.50
        static let BreezoApiKey: String = "68fccdf466ac4aa6b8c467ea93870d9f"
    }

    struct Keys {
        struct RemoteConfig {
            static let MaximumParkLifeInMinutes: String                = "MaximumParkLifeInMinutes"
        }
        
        static let NoNoAnimation: String                = "noAnimation" // not using inferred on purpose, to help Swift compiler
        struct Persistency {
            static let PermissionRequestCounter: String = "PermissionRequestCounter"
            static let LastCrash: String                = "last crash"
        }
    }

    let GoogleMapsWebApiKey: String
    let GoogleMapsMobileSdkApiKey: String
    private(set) var maximumParkLifeInMinutes: Int
    
    var shouldMockLocationAndData: Bool
    var shouldShowBaqiMarkers: Bool
    
    //private var remoteConfig: RemoteConfig

    private init() {
        //remoteConfig = RemoteConfig.remoteConfig()

        maximumParkLifeInMinutes = 30

        shouldMockLocationAndData = true
        shouldShowBaqiMarkers = false

//        if let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true) {
//            remoteConfig.configSettings = remoteConfigSettings
//        }

        guard let secretConstantsPlistFilePath: String = Bundle.main.path(forResource: "SecretConstants", ofType: "plist"),
        let config: [String: String] = NSDictionary(contentsOfFile: secretConstantsPlistFilePath) as? [String : String],
        let googleMapsWebApiKey = config["GoogleMapsWebApiKey"],
        let googleMapsMobileSdkApiKey = config["GoogleMapsMobileSdkApiKey"]
        else { fatalError("No way! The app must have this plist file with the mandatory keys") }

        GoogleMapsWebApiKey = googleMapsWebApiKey
        GoogleMapsMobileSdkApiKey = googleMapsMobileSdkApiKey
    }
//    func fetchRemoteConfig() {
//        remoteConfig.fetch(withExpirationDuration: 2, completionHandler: { [weak self] (fetchStatus, error) -> () in
//            if fetchStatus == .success {
//                if let maximumParkLifeInMinutes = self?.remoteConfig[Keys.RemoteConfig.MaximumParkLifeInMinutes].numberValue?.intValue, maximumParkLifeInMinutes > 0 {
//                    self?.maximumParkLifeInMinutes = maximumParkLifeInMinutes
//                }
//            } else if let error = error {
//                📕("Failed to fetch remote config! Error: \(error)")
//            } else {
//                📕("Failed to fetch remote config! Missing error object...")
//            }
//        })
//    }
}

extension UIColor {
    convenience init(withBaqi baqi: Int?) {
        guard let baqi = baqi, baqi >= 0 else { self.init(hexString: UIColor.appGrayHexa); return }
        
        switch baqi {
        case 80...100:
            self.init(hexString: UIColor.baqiExcellentHexa_1)
        case 60...80:
            self.init(hexString: UIColor.baqiGoodHexa_2)
        case 40...60:
            self.init(hexString: UIColor.baqiModerateHexa_3)
        case 20...40:
            self.init(hexString: UIColor.baqiLowHexa_4)
        case 0...20:
            self.init(hexString: UIColor.baqiPoorHexa_5)
        default:
            self.init(hexString: UIColor.appGrayHexa)
        }
    }
    
    static var baqiExcellentHexa_1: String {
        return "#00c853"
    }
    
    static var baqiGoodHexa_2: String {
        return "#6ad555"
    }
    
    static var baqiModerateHexa_3: String {
        return "#ffd741"
    }
    
    static var baqiLowHexa_4: String {
        return "#ff7338"
    }
    
    static var baqiPoorHexa_5: String {
        return "#ff3b30"
    }
    
    static var appGrayHexa: String {
        return "#888888"
    }
    
}
