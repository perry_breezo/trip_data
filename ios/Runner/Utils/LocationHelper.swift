//
//  LocationHelper.swift
//  JobInterviewHW2.0
//
//  Created by Perry on 01/12/2017.
//  Copyright © 2017 perrchick. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationCoordinate: Codable {
    var latitude: Double
    var longitude: Double
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

protocol LocationHelperDelegate: class {
    func onLocationUpdated(updatedLocation: CLLocation)
}

class LocationHelper: NSObject, CLLocationManagerDelegate {
    static let shared: LocationHelper = LocationHelper()

    weak var delegate: LocationHelperDelegate?
    private(set) var currentLocation: CLLocation?
    lazy var locationManager: CLLocationManager = {
        let locationManager: CLLocationManager = CLLocationManager();
        locationManager.delegate = self
        return locationManager
    }()

    override init() {
        super.init()

        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)

        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
    }

    var distanceFilter: CLLocationDistance {
        get {
            return locationManager.distanceFilter
        }
        set {
            locationManager.distanceFilter = newValue
        }
    }

    func startUpdate() {
        guard isPermissionGranted else { return }
        locationManager.startUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = true
    }

    func stopUpdate() {
        locationManager.stopUpdatingLocation()
        //locationManager.allowsBackgroundLocationUpdates = false
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
            delegate?.onLocationUpdated(updatedLocation: location)
        }
    }

    var isPermissionGranted: Bool {
        return CLLocationManager.authorizationStatus() == .authorizedWhenInUse
    }

    @objc func applicationWillEnterForeground(notification: Notification) {
        startUpdate()
    }

    @objc func applicationDidEnterBackground(notification: Notification) {
        stopUpdate()
    }

//    static func fetchNearByPlaces(aroundLocation location: CLLocationCoordinate2D, withRadius radius: Float, resultCallback: @escaping CallbackClosure<(location: CLLocationCoordinate2D, places: [Place])?>) {
//
//        let urlString = String(format: Communicator.API.RequestUrls.NearByPlacesFormat,
//                               location.latitude,
//                               location.longitude,
//                               radius,
//                               Configurations.shared.GoogleMapsWebApiKey)
//
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        Communicator.request(urlString: urlString) { (response) in
//            guard let response = response else { resultCallback((location: location, places: [])); return }
//            var placesResult: [Place] = [Place]()
//            switch response {
//            case .succeeded(let json):
//                if let jsonDictionary = (json as? RawJsonFormat),
//                    let placesJsonArray = jsonDictionary[Communicator.API.ResponseKeys.GoogleMapsResults] as? [RawJsonFormat] {
//                    for placeJson in placesJsonArray {
//                        if let place = Place.from(json: placeJson) {
//                            placesResult.append(place)
//                        }
//                    }
//                }
//            case .failed(let message):
//                📕("request failed, message: \(message)")
//            }
//
//            UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            resultCallback((location: location, places: placesResult))
//        }
//    }
    
    var isCarSpeed: Bool {
        return (currentLocation?.speed).or(0) > 5
    }

    var isAlmostIdle: Bool {
        return (currentLocation?.speed).or(0) < 1
    }
    
//    static func fetchAutocompleteSuggestions(forPhrase keyword: String, predictionsResultCallback: @escaping CallbackClosure<(keyword: String, predictions: [Prediction])?>) {
//        guard let urlQueryKeyword = keyword.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
//            predictionsResultCallback(nil)
//            return
//        }
//        let urlString = String(format: Communicator.API.RequestUrls.AutocompletePlacesFormat,
//                               urlQueryKeyword,
//                               Configurations.shared.GoogleMapsWebApiKey)
//
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        Communicator.request(urlString: urlString) { (response) in
//            var predictionsResult: [Prediction] = [Prediction]()
//            guard let response = response else { predictionsResultCallback((keyword: keyword, predictions: predictionsResult)); return }
//
//            switch response {
//            case .succeeded(let json):
//                if let jsonDictionary = (json as? RawJsonFormat),
//                    let jsonArray = jsonDictionary[Communicator.API.ResponseKeys.GoogleMapsPredictions] as? [RawJsonFormat] {
//                    for predictionJsonDictionary in jsonArray {
//                        guard let description = predictionJsonDictionary[AddressPrediction.InterpretationKeys.Description] as? String,
//                            let placeId = predictionJsonDictionary[AddressPrediction.InterpretationKeys.PlaceId] as? String
//                            else { continue }
//                        let prediction = AddressPrediction(placeId: placeId, addressDescription: description)
//                        predictionsResult.append(prediction)
//                    }
//
//
//                    // So glad we're developing in Swift ツ 👆
//
//                }
//            case .failed(let message):
//                📕("request failed, message: \(message)")
//            }
//
//            UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            predictionsResultCallback((keyword: keyword, predictions: predictionsResult))
//        }
//    }

    func requestPermissionsIfNeeded() {
        let counterKey: String = Configurations.Keys.Persistency.PermissionRequestCounter;
        let permissionRequestCounter: Int = UserDefaults.load(key: counterKey, defaultValue: 0)
        if permissionRequestCounter > 0 {
            if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(settingsUrl)
                    }
                }
            }
        } else {
            // First time for this life time
            locationManager.requestWhenInUseAuthorization()
        }

        UserDefaults.save(value: permissionRequestCounter + 1, forKey: Configurations.Keys.Persistency.PermissionRequestCounter).synchronize()
    }

    private static func parseGeocodeResponse(_ responseObject: Any) -> String? {
        var result: String?
        
        guard let responseDictionary = responseObject as? RawJsonFormat,
            let status = responseDictionary["status"] as? String, status == "OK" else { return result }
        
        if let results = responseDictionary["results"] as? [AnyObject],
            let firstPlace = results[0] as? [String:AnyObject],
            let firstPlaceName = firstPlace["formatted_address"] as? String {
            result = firstPlaceName
        }
        
        return result
    }
}
