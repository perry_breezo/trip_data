//
//  Response.swift
//  Runner
//
//  Created by Perry Shalom on 03/01/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation

// Reference: http://nshipster.com/swift-operators/
func == (left: AirQualityData, right: AirQualityData) -> Bool {
    let pointer1 = Unmanaged<AnyObject>.passUnretained(left as AnyObject).toOpaque()
    let pointer2 = Unmanaged<AnyObject>.passUnretained(right as AnyObject).toOpaque()
    return pointer1 == pointer2
}

class AirQualityData: DictionaryConvertible, CustomStringConvertible {
    static private var _dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return df
    }()

    //let originalJson: [NSString:NSObject]
    /// A computed variable that generates a readable human date
    var datetime: String {
        return AirQualityData._dateFormatter.string(from: timestamp.date)
    }
    private(set) var locationCoordinate: LocationCoordinate // could be `let` as well.
    private(set) var timestamp: Timestamp
    private(set) var baqi: Int

    private(set) var coTitle: String?
    private(set) var coUnits: String?
    private(set) var coValue: Float?

    private(set) var no2Title: String?
    private(set) var no2Units: String?
    private(set) var no2Value: Float?

    private(set) var o3Title: String?
    private(set) var o3Units: String?
    /// Ozone value
    private(set) var o3Value: Float?

    private(set) var pm10Title: String?
    private(set) var pm10Units: String?
    private(set) var pm10Value: Float?
    
    private(set) var pm25Title: String?
    private(set) var pm25Units: String?
    private(set) var pm25Value: Float?

    private(set) var so2Title: String?
    private(set) var so2Units: String?
    private(set) var so2Value: Float?

    required convenience init?(json: RawJsonFormat) {
        guard let data = json["data"] as? RawJsonFormat,
            let datetime = data["datetime"] as? String,
            let indexes = data["indexes"] as? RawJsonFormat,
            let baqiJson = indexes["baqi"] as? RawJsonFormat,
            let date = AirQualityData._dateFormatter.date(from: datetime),
            let baqi = baqiJson["aqi"] as? Int else { return nil }

        let _locationCoordinate: LocationCoordinate
        if let locationJson = data["location"] as? RawJsonFormat {
            let lat = locationJson["latitude"] as? Double ?? 0
            let lng = locationJson["longitude"] as? Double ?? 0
            _locationCoordinate = LocationCoordinate(latitude: lat, longitude: lng)
        } else {
            _locationCoordinate = LocationCoordinate(latitude: 0, longitude: 0)
            📕("Failed to find locaiton JSON in response: \(json)")
        }

        let reportedBaqi = Configurations.shared.shouldMockLocationAndData ? 10 ~ 90 : baqi
        self.init(timestamp: date.timestamp, locationCoordinate: _locationCoordinate, baqi: reportedBaqi)

        let pollutants = data["pollutants"] as? RawJsonFormat

        let co = pollutants?["co"] as? RawJsonFormat
        coTitle = co?["display_name"] as? String
        let coConcentration = co?["concentration"] as? RawJsonFormat
        coValue = coConcentration?["value"] as? Float
        coUnits = coConcentration?["units"] as? String

        let no2 = pollutants?["no2"] as? RawJsonFormat
        no2Title = no2?["display_name"] as? String
        let no2Concentration = no2?["concentration"] as? RawJsonFormat
        no2Value = no2Concentration?["value"] as? Float
        no2Units = no2Concentration?["units"] as? String

        let o3Json = pollutants?["o3"] as? RawJsonFormat // Ozone
        o3Title = o3Json?["display_name"] as? String
        let o3Concentration = o3Json?["concentration"] as? RawJsonFormat
        o3Value = o3Concentration?["value"] as? Float
        o3Units = o3Concentration?["units"] as? String

        let pm10Json = pollutants?["pm10"] as? RawJsonFormat
        pm10Title = pm10Json?["display_name"] as? String
        let pm10Concentration = pm10Json?["concentration"] as? RawJsonFormat
        pm10Value = pm10Concentration?["value"] as? Float
        pm10Units = pm10Concentration?["units"] as? String
        
        let pm25Json = pollutants?["pm25"] as? RawJsonFormat
        pm25Title = pm25Json?["display_name"] as? String
        let pm25Concentration = pm25Json?["concentration"] as? RawJsonFormat
        pm25Value = pm25Concentration?["value"] as? Float
        pm25Units = pm25Concentration?["units"] as? String

        let so2Json = pollutants?["so2"] as? RawJsonFormat
        so2Title = so2Json?["display_name"] as? String
        let so2Concentration = so2Json?["concentration"] as? RawJsonFormat
        so2Value = so2Concentration?["value"] as? Float
        so2Units = so2Concentration?["units"] as? String
    }
    
    init(timestamp: Timestamp, locationCoordinate: LocationCoordinate, baqi: Int) {
        self.locationCoordinate = locationCoordinate
        self.timestamp = timestamp
        self.baqi = baqi
    }

    func `import`(fromOther other: AirQualityData) {
        locationCoordinate = other.locationCoordinate
        timestamp = other.timestamp
        baqi = other.baqi

        coTitle = other.coTitle
        coUnits = other.coUnits
        coValue = other.coValue
        
        no2Title = other.no2Title
        no2Units = other.no2Units
        no2Value = other.no2Value
        
        o3Title = other.o3Title
        o3Units = other.o3Units
        o3Value = other.o3Value
        
        pm10Title = other.pm10Title
        pm10Units = other.pm10Units
        pm10Value = other.pm10Value
        
        pm25Title = other.pm25Title
        pm25Units = other.pm25Units
        pm25Value = other.pm25Value
        
        so2Title = other.so2Title
        so2Units = other.so2Units
        so2Value = other.so2Value

    }

    var description: String {
        return "timestamp: \(timestamp) (\(datetime)), baqi: \(baqi)"
    }

    var uiDescription: String {
        return "locaiton: \(locationCoordinate), baqi: \(baqi)"
    }

}

/*

{
    "metadata": null,
    "data": {
        "datetime": "2018-12-30T11:00:00Z",
        "data_available": true,
        "indexes": {
            "baqi": {
                "display_name": "BreezoMeter AQI",
                "aqi": 66,
                "aqi_display": "66",
                "color": "#9CD828",
                "category": "Good air quality",
                "dominant_pollutant": "pm25"
            },
            "usa_epa": {
                "display_name": "AQI (US)",
                "aqi": 62,
                "aqi_display": "62",
                "color": "#FFFF00",
                "category": "Moderate air quality",
                "dominant_pollutant": "pm25"
            }
        },
        "pollutants": {
            "co": {
                "display_name": "CO",
                "full_name": "Carbon monoxide",
                "aqi_information": {
                    "baqi": {
                        "display_name": "BreezoMeter AQI",
                        "aqi": 91,
                        "aqi_display": "91",
                        "color": "#009E3A",
                        "category": "Excellent air quality"
                    }
                },
                "concentration": {
                    "value": 1053.55,
                    "units": "ppb"
                },
                "sources_and_effects": {
                    "sources": "Typically originates from incomplete combustion of carbon fuels, such as that which occurs in car engines and power plants.",
                    "effects": "When inhaled, carbon monoxide can prevent the blood from carrying oxygen. Exposure may cause dizziness, nausea and headaches. Exposure to extreme concentrations can lead to loss of consciousness."
                }
            },
            "no2": {
                "display_name": "NO2",
                "full_name": "Nitrogen dioxide",
                "aqi_information": {
                    "baqi": {
                        "display_name": "BreezoMeter AQI",
                        "aqi": 82,
                        "aqi_display": "82",
                        "color": "#009E3A",
                        "category": "Excellent air quality"
                    }
                },
                "concentration": {
                    "value": 24.2,
                    "units": "ppb"
                },
                "sources_and_effects": {
                    "sources": "Main sources are fuel burning processes, such as those used in industry and transportation.",
                    "effects": "Exposure may cause increased bronchial reactivity in patients with asthma, lung function decline in patients with COPD, and increased risk of respiratory infections, especially in young children."
                }
            },
            "o3": {
                "display_name": "O3",
                "full_name": "Ozone",
                "aqi_information": {
                    "baqi": {
                        "display_name": "BreezoMeter AQI",
                        "aqi": 95,
                        "aqi_display": "95",
                        "color": "#009E3A",
                        "category": "Excellent air quality"
                    }
                },
                "concentration": {
                    "value": 7.04,
                    "units": "ppb"
                },
                "sources_and_effects": {
                    "sources": "Ozone is created in a chemical reaction between atmospheric oxygen, nitrogen oxides, carbon monoxide and organic compounds, in the presence of sunlight.",
                    "effects": "Ozone can irritate the airways and cause coughing, a burning sensation, wheezing and shortness of breath. Additionally, ozone is one of the major components of photochemical smog."
                }
            },
            "pm10": {
                "display_name": "PM10",
                "full_name": "Inhalable particulate matter (<10µm)",
                "aqi_information": {
                    "baqi": {
                        "display_name": "BreezoMeter AQI",
                        "aqi": 77,
                        "aqi_display": "77",
                        "color": "#84CF33",
                        "category": "Good air quality"
                    }
                },
                "concentration": {
                    "value": 25.98,
                    "units": "ug/m3"
                },
                "sources_and_effects": {
                    "sources": "Main sources are combustion processes (e.g. indoor heating, wildfires), mechanical processes (e.g. construction, mineral dust, agriculture) and biological particles (e.g. pollen, bacteria, mold).",
                    "effects": "Inhalable particles can penetrate into the lungs. Short term exposure can cause irritation of the airways, coughing, and aggravation of heart and lung diseases, expressed as difficulty breathing, heart attacks and even premature death."
                }
            },
            "pm25": {
                "display_name": "PM2.5",
                "full_name": "Fine particulate matter (<2.5µm)",
                "aqi_information": {
                    "baqi": {
                        "display_name": "BreezoMeter AQI",
                        "aqi": 66,
                        "aqi_display": "66",
                        "color": "#84CF33",
                        "category": "Good air quality"
                    }
                },
                "concentration": {
                    "value": 21.11,
                    "units": "ug/m3"
                },
                "sources_and_effects": {
                    "sources": "Main sources are combustion processes (e.g. power plants, indoor heating, car exhausts, wildfires), mechanical processes (e.g. construction, mineral dust) and biological particles (e.g. bacteria, viruses).",
                    "effects": "Fine particles can penetrate into the lungs and bloodstream. Short term exposure can cause irritation of the airways, coughing and aggravation of heart and lung diseases, expressed as difficulty breathing, heart attacks and even premature death."
                }
            },
            "so2": {
                "display_name": "SO2",
                "full_name": "Sulfur dioxide",
                "aqi_information": {
                    "baqi": {
                        "display_name": "BreezoMeter AQI",
                        "aqi": 96,
                        "aqi_display": "96",
                        "color": "#009E3A",
                        "category": "Excellent air quality"
                    }
                },
                "concentration": {
                    "value": 4.45,
                    "units": "ppb"
                },
                "sources_and_effects": {
                    "sources": "Main sources are burning processes of sulfur-containing fuel in industry, transportation and power plants.",
                    "effects": "Exposure causes irritation of the respiratory tract, coughing and generates local inflammatory reactions. These in turn, may cause aggravation of lung diseases, even with short term exposure."
                }
            }
        },
        "health_recommendations": {
            "general_population": "With this level of air quality, you have no limitations. Enjoy the outdoors!",
            "elderly": "If you start to feel respiratory discomfort such as coughing or breathing difficulties, consider reducing the intensity of your outdoor activities. Try to limit the time you spend near busy roads, construction sites, open fires and other sources of smoke.",
            "lung_diseases": "If you start to feel respiratory discomfort such as coughing or breathing difficulties, consider reducing the intensity of your outdoor activities. Try to limit the time you spend near busy roads, industrial emission stacks, open fires and other sources of smoke.",
            "heart_diseases": "If you start to feel respiratory discomfort such as coughing or breathing difficulties, consider reducing the intensity of your outdoor activities. Try to limit the time you spend near busy roads, construction sites, industrial emission stacks, open fires and other sources of smoke.",
            "active": "If you start to feel respiratory discomfort such as coughing or breathing difficulties, consider reducing the intensity of your outdoor activities. Try to limit the time you spend near busy roads, construction sites, industrial emission stacks, open fires and other sources of smoke.",
            "pregnant_women": "To keep you and your baby healthy, consider reducing the intensity of your outdoor activities. Try to limit the time you spend near busy roads, construction sites, open fires and other sources of smoke.",
            "children": "If you start to feel respiratory discomfort such as coughing or breathing difficulties, consider reducing the intensity of your outdoor activities. Try to limit the time you spend near busy roads, construction sites, open fires and other sources of smoke."
        }
    },
    "error": null
}
*/

//extension Timestamp {
//    static func from(string: String) -> Timestamp? {
//        guard let timestamp = UInt64(string) else { return nil }
//
//        return timestamp
//    }
//}

extension Date {
    var timestamp: Timestamp {
        return Timestamp(timeIntervalSince1970 * 1000)
    }
    
}

extension Timestamp {
    var date: Date {
        return Date(timeIntervalSince1970: TimeInterval(self) / 1000)
    }
    
}
