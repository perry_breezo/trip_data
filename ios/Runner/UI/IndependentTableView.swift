//
//  ResultsViewController.swift
//  Runner
//
//  Created by Perry Shalom on 06/01/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import UIKit

//class CellClass<ItemClass>: UITableViewCell {
//    func configure(item: ItemClass) {
//        fatalError("you must override 'configure' method!")
//    }
//}

class IndependentTableView<ItemClass, CellClass: UITableViewCell>: UITableView, UITableViewDelegate, UITableViewDataSource {
    var data: [ItemClass]!
    var CellReuseIdentifier: String!

    override func reloadData() {
        if delegate == nil {
            delegate = self
        }
        if dataSource == nil {
            dataSource = self
        }

        super.reloadData()
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier, for: indexPath) as! CellClass
//        cell.configure(item: data[indexPath.row])

        return cell
    }

}
