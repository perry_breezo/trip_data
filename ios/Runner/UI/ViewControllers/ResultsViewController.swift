//
//  ResultsViewController.swift
//  Runner
//
//  Created by Perry Shalom on 06/01/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    var data: [AirQualityData]!
    //var tableView: IndependentTableView<AirQualityData, AirQualityCell>!

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        //tableView = IndependentTableView(frame: view.frame)
        //view.addSubview(tableView)
        //tableView.stretchToSuperViewEdges()
        //tableView.data = aqConditions

        tableView.delegate = self
        tableView.dataSource = self

//        view.onSwipe(direction: UISwipeGestureRecognizerDirection.down) { [weak self] _ in
//            self?.dismiss(animated: true, completion: nil)
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Air Quality Results".localized()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if data == nil {
            data = []
        }
    }
}

class AirQualityCell: UITableViewCell {
    static let CellReuseIdentifier: String = "AirQualityCell"
    func configure(item: AirQualityData) {
        textLabel?.text = "\(item.baqi)"
        detailTextLabel?.text = "\(item.locationCoordinate)"
    }
}

extension ResultsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let aqData = data[indexPath.row]
        UIAlertController.makeAlert(title: "Details", message: aqData.uiDescription)
            .withAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            .show()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ResultsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AirQualityCell.CellReuseIdentifier, for: indexPath) as! AirQualityCell
        cell.configure(item: data[indexPath.row])
        
        return cell
    }
}
//class AirQualityCell: IndependentTableView<AirQualityData, AirQualityCell>.CellClass {
//    override func configure(item: AirQualityData) {
//        textLabel?.text = "\(item.baqi)"
//        detailTextLabel?.text = "\(item.locationCoordinate)"
//    }
//}
