//
//  AQRecorderViewController.swift
//  Runner
//
//  Created by Perry Shalom on 30/12/2018.
//  Copyright © 2018 The Chromium Authors. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class AQRecorderViewController: UIViewController, LocationHelperDelegate {
    lazy var locationHelper = LocationHelper.shared
    lazy var airQualities = [AirQualityData]()

    lazy var regionRadius: CLLocationDistance = 100
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var txtDetails: UITextView!

    lazy var initialMockMovementLatitude: CLLocationDegrees = 32.337398
    lazy var initialMockMovementLongitude: CLLocationDegrees = 34.855633
    lazy var mockMovement: ClosureTimer = {
        return ClosureTimer(afterDelay: 4, userInfo: nil, repeats: true) { [weak self] (tuple) in
            guard let strongSelf = self else { return }
            let timer = tuple.0
            //📗(timer.counter)
            let addition = Double(timer.counter) / 1000
            let location: CLLocation = CLLocation(latitude: strongSelf.initialMockMovementLatitude + addition, longitude: strongSelf.initialMockMovementLongitude + addition)

            self?.onLocationUpdated(updatedLocation: location)
        }
    }()

    var isRecording: Bool = false {
        didSet {
            if isRecording {
                btnRecord.setTitle("✋🏼", for: UIControlState.normal)
            } else {
                if airQualities.count > 0 {
                    PerrFuncs.runOnBackground {
                        self.airQualities.remove(where: { $0.timestamp == 0 } )
                        DataManager.saveJson(withConditions: self.airQualities)
                    }
                    UIAlertController.makeAlert(title: "Done recording", message: "Would you like to see the results?")
                        .withAction(UIAlertAction(title: "See results", style: UIAlertActionStyle.default, handler: { [weak self] (action) in
                            guard self?.airQualities.count ?? 0 > 0 else { return }
                            
                            let resultsViewController: ResultsViewController = ResultsViewController.instantiate()
                            resultsViewController.data = self?.airQualities
                            //present(resultsViewController, animated: true, completion: nil)
                            self?.navigationController?.pushViewController(resultsViewController, animated: true)
                        }))
                        .withAction(UIAlertAction(title: "Pass", style: UIAlertActionStyle.cancel, handler: nil))
                        .show()
                }
                btnRecord.setTitle("🏁", for: UIControlState.normal)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        txtDetails.delegate = self
        locationHelper.delegate = self
        locationHelper.distanceFilter = 300
        if Configurations.shared.shouldMockLocationAndData || PerrFuncs.isRunningOnSimulator() {
            📗(mockMovement.block)
        } else {
            locationHelper.startUpdate()
        }

        txtDetails.onLongPress { (onLongPressListener) in
            guard onLongPressListener.state == .began else { return }

            UIAlertController.makeAlert(title: "BAQI markers", message: "Should we present BAQI markers?")
                .withAction(UIAlertAction(title: "Hide", style: UIAlertActionStyle.destructive, handler: { [weak self] _ in
                    Configurations.shared.shouldShowBaqiMarkers = false
                    self?.txtDetails.text = Configurations.shared.shouldShowBaqiMarkers ? "presenting BAQI markers" : "hiding BAQI markers"
                }))
                .withAction(UIAlertAction(title: "Present", style: UIAlertActionStyle.default, handler: { [weak self] _ in
                    Configurations.shared.shouldShowBaqiMarkers = true
                    self?.txtDetails.text = Configurations.shared.shouldShowBaqiMarkers ? "presenting BAQI markers" : "hiding BAQI markers"
                }))
                .show()
        }
        txtDetails.isPresented = false

        isRecording = false
        btnRecord.onClick { [weak self] onClickListener in
            //guard let strongSelf = self else { return }

            //strongSelf.isRecording = !strongSelf.isRecording
            FLEXManager.shared()?.showExplorer()
        }

       view.onSwipe(direction: UISwipeGestureRecognizerDirection.down) { [weak self] _ in
           self?.dismiss(animated: true, completion: nil)
       }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Native Screen".localized()
        
        txtDetails.text = Configurations.shared.shouldShowBaqiMarkers ? "presenting BAQI markers" : "hiding BAQI markers"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if airQualities.count > 0 {
            UIAlertController.makeAlert(title: "Reset?".localized(), message: "Reset previous recorded air quality details?".localized())
                .withAction(UIAlertAction(title: "Reset list".localized(), style: UIAlertActionStyle.default, handler: { [weak self] action in
                    self?.airQualities.removeAll()
                    if let annotations = self?.mapView.annotations {
                        self?.mapView.removeAnnotations(annotations)
                    }
                    if let overlays = self?.mapView.overlays {
                        self?.mapView.removeOverlays(overlays)
                    }
                }))
                .withAction(UIAlertAction(title: "No".localized(), style: UIAlertActionStyle.cancel, handler: nil))
                .show()
        }
    }
    
    func onLocationUpdated(updatedLocation: CLLocation) {
        guard isRecording else { return }

        //TODO: check if the radius diff is significant
        let placeHolder = AirQualityData(timestamp: 0, locationCoordinate: updatedLocation.coordinate.toLocationCoordinate(), baqi: 0)
        let previous = airQualities.last
        airQualities.append(placeHolder)
        📗("updatedLocation: \(updatedLocation)")
        DataManager.fetchBaqi(latitude: updatedLocation.coordinate.latitude, longitude: updatedLocation.coordinate.longitude, completion: { [weak self] aqData in
            📗(aqData)
            if let aqData = aqData {
                placeHolder.import(fromOther: aqData)
                PerrFuncs.runOnUiThread(block: {
                    guard let strongSelf = self else { return }
                    strongSelf.addToMap(aqData: aqData, after: previous)
                    //self?.moveMapCamera(toLocation: updatedLocation)
                    strongSelf.mapView.showAnnotations(strongSelf.mapView.annotations, animated: true)
                })
            } else {
                self?.airQualities.remove(where: { $0 == placeHolder })
            }
        })
    }
    
    func moveMapCamera(toLocation location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    func addToMap(aqData: AirQualityData, after previous: AirQualityData?) {
        let annotation = BaqiAnnotation(data: aqData)

        if let previous = previous {
            let polyline = GradientPolylineOverlay(qualities: [previous, aqData])
            self.mapView.add(polyline)
        }

        mapView.addAnnotation(annotation)
    }
}

extension CLLocationCoordinate2D {
    func toLocationCoordinate() -> LocationCoordinate {
        return LocationCoordinate(latitude: latitude, longitude: longitude)
    }
}

extension LocationCoordinate {
    func toLocationCoordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

extension AQRecorderViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let polyLineRender: MKOverlayRenderer = (overlay as? GradientPolylineOverlay)?.renderer(withLineWidth: 20) ?? MKOverlayRenderer()
        return polyLineRender
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: MKAnnotationView!

        let baqiAnnotation = annotation as! BaqiAnnotation
        let baqiMarker: BaqiAnnotationView

        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: BaqiAnnotationView.AnnotationIdentifier) {
            dequeuedAnnotationView.annotation = baqiAnnotation
            annotationView = dequeuedAnnotationView
            let baqiMarkerView = annotationView.subviews.filter( { $0 is BaqiAnnotationView } )
            baqiMarker = baqiMarkerView.first as! BaqiAnnotationView
        } else {
            // Failed to dequeue -> instantiate
//            if #available(iOS 11.0, *) {
//                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: MyAnnotationViewIdentifier)
//            } else { // Fallback on earlier versions
//                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: MyAnnotationViewIdentifier)
//            }
            
            annotationView = MKAnnotationView(annotation: baqiAnnotation, reuseIdentifier: BaqiAnnotationView.AnnotationIdentifier)
            baqiMarker = BaqiAnnotationView.instantiateFromNib()
            annotationView.addSubview(baqiMarker)
        }

        // Configure marker
        baqiMarker.bgColor = UIColor(withBaqi: baqiAnnotation.value)
        baqiMarker.text = "\(baqiAnnotation.value)"
        baqiMarker.stretchToSuperViewEdges()
        annotationView.tintColor = UIColor(withBaqi: baqiAnnotation.value)
        annotationView.canShowCallout = true
        
        baqiMarker.isPresented = Configurations.shared.shouldShowBaqiMarkers ? true : false

        return annotationView
    }
}

//extension CLLocation {
//    convenience init(lat: CLLocationDegrees, lng: CLLocationDegrees) {
//        self.init(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), altitude: 0, horizontalAccuracy: 5, verticalAccuracy: 5, course: CLLocationDirection(), speed: 0, timestamp: Date())
//    }
//}

extension AQRecorderViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }
}

class BaqiAnnotation: MKPointAnnotation {
    let value: Int
    
    init(data: AirQualityData) {
        self.value = data.baqi
        super.init()

        title = "BAQI: \(data.baqi)"
        //subtitle = "BAQI: \(aqData.baqi)"
        coordinate = data.locationCoordinate.toLocationCoordinate2D()
    }
}
