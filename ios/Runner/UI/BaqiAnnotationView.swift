//
//  BaqiAnnotationView.swift
//  SomeApp
//
//  Created by Perry on 01/04/2016.
//  Copyright © 2016 PerrchicK. All rights reserved.
//

import Foundation
import MapKit

class BaqiAnnotationView: UIView {
    static let AnnotationIdentifier: String = "BaqiAnnotationView"
    @IBOutlet weak var annotationIconLabel: UILabel!

    override func awakeFromNib() {
        annotationIconLabel.font = annotationIconLabel.font.withSize(20)
        annotationIconLabel.makeRoundedCorners()
        isUserInteractionEnabled = false
    }
    
    var bgColor: UIColor? {
        set {
            annotationIconLabel.backgroundColor = newValue
        }
        get {
            return annotationIconLabel.backgroundColor
        }
    }
    
    var text: String? {
        set {
            annotationIconLabel.text = newValue
        }
        get {
            return annotationIconLabel.text
        }
    }

//    func generateIcon() -> String {
//        let randomIndex = 0 ~ possibleIcons.count
//        return possibleIcons[randomIndex]
//    }
}
