import UIKit
import Flutter
import GoogleMaps
import CoreData

//TODO: Perry's subtasks
/*
 save files, filename: "start-time-{FROM}-{TO}"
 {
 startTime: "",
 endTime:"",
 conditions:[{time: ...}]
 }
 */


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {

    private(set) var isConnectedToTheInternet = true {
        didSet {
            if isConnectedToTheInternet {
                //NotificationCenter.
            }
        }
    }

    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    private lazy var onFlutterCall: ((FlutterMethodCall, FlutterResult) -> Void) = {
        return { (call: FlutterMethodCall, result: FlutterResult) -> Void in
            switch call.method {
            case "open_maps":
                UIAlertController.makeActionSheet(title: "Go to native screen?", message: "This is a native 'menu'.")
//                    .withAction(UIAlertAction(title: "Real", style: UIAlertActionStyle.default, handler: { _ in
//                        Configurations.shared.shouldMockLocationAndData = false
//                        UIApplication.mostTopViewController()?.present(UINavigationController(rootViewController: AQRecorderViewController.instantiate()), animated: true, completion: nil)
//                    }))
                    .withAction(UIAlertAction(title: "Go...", style: UIAlertActionStyle.default, handler: { _ in
                        Configurations.shared.shouldMockLocationAndData = true
                        UIApplication.mostTopViewController()?.present(UINavigationController(rootViewController: AQRecorderViewController.instantiate()), animated: true, completion: nil)
                    }))
                    .withAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    .show()
                result("1")
            default:
                result("0") // Never return nil!!
            }
        }
    }()

    override func application (
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
        ) -> Bool {
        GMSServices.provideAPIKey("AIzaSyChgTTfM3JsXfmIqjcyUP1g-UVUTqVEZBk")
        GeneratedPluginRegistrant.register(with: self)

        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let nativeChannel = FlutterMethodChannel(name: "main.breezometer/native_channel", binaryMessenger: controller)

        nativeChannel.setMethodCallHandler(onFlutterCall)

        Reachability.shared?.whenReachable = { _ in
            PerrFuncs.runOnUiThread(block: {
                AppDelegate.shared.isConnectedToTheInternet = true
            })
        }
        Reachability.shared?.whenUnreachable = { _ in
            PerrFuncs.runOnUiThread(block: {
                AppDelegate.shared.isConnectedToTheInternet = false
            })
        }
        try? Reachability.shared?.startNotifier()

        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    override func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        super.application(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }

    
    // MARK: - Core Data stack

    // Lazy instantiation variable - will be allocated (and initialized) only once
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.perrchick.SomeApp" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "SomeApp", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch let error {
            // Report any error we got.
            let wrappedError = NSError.create(errorDomain: "YOUR_ERROR_DOMAIN", errorCode: 9999, description: "Failed to initialize the application's saved data", failureReason: "There was an error creating or loading the application's saved data.", underlyingError: error)

            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator: NSPersistentStoreCoordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
//    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
//        if motion == .motionShake {
//            ToastMessage.show(messageText: "Boom! shake, shake, shake the room...")
//        }
//    }
}
