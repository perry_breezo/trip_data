//
//  DataManager.swift
//  SomeApp
//
//  Created by Perry on 3/17/16.
//  Copyright © 2016 PerrchicK. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DataManager {
    static var applicationLibraryPath: NSString = {
        if let libraryDirectoryPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).last {
            return libraryDirectoryPath as NSString
        }
        
        📕("ERROR!! Library directory not found 😱")
        return ""
    }()

    static var applicationDocumentsPath: URL = {
        guard let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first else {
            📕("ERROR!! Library directory not found 😱")
            return URL(fileURLWithPath: "")
        }

        return documentsDirectory
    }()

    fileprivate static var managedContext: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
    }

    @discardableResult
    static func saveImage(_ imageToSave: UIImage, toFile filename: String) -> Bool {
        if let data = UIImagePNGRepresentation(imageToSave) {
            do {
                try data.write(to: URL(fileURLWithPath: (applicationLibraryPath as String) + "/" + filename), options: .atomicWrite)
                return true
            } catch {
                📕("Failed to save image!")
            }
        }

        return false
    }

    /// This method blocks the current thread and downloads the image from this URL string in case the URL is valid
    static func downloadImage(fromUrl imageUrlString: String) -> UIImage? {
        if let imageUrl = URL(string: imageUrlString), let imageData = try? Data(contentsOf: imageUrl) {
            return UIImage(data: imageData)
        }
        
        return nil
    }
    
    static func loadImage(fromFile filename: String) -> UIImage? {
        if let data = try? Data(contentsOf: URL(fileURLWithPath: (applicationLibraryPath as String) + "/" + filename)) {
            return UIImage(data: data)
        }

        return nil
    }

    static func saveJson(withConditions conditions: [AirQualityData]) -> String {
        let _objectData: Data? = try? JSONEncoder().encode(conditions)
        guard let objectData = _objectData else { return "" }
        if let jsonString = String(data: objectData, encoding: .utf8) {
            return jsonString
        } else {
            📕("Failed to serizlize JSON from conditions array: \(conditions)")
            return ""
        }
    }
    
    static func fetchBaqi(latitude: Double, longitude: Double, completion: @escaping CallbackClosure<AirQualityData?>) {
        Communicator.fetchBaqi(latitude: latitude, longitude: longitude, completion: { json in
            guard let json = json, let aqData: AirQualityData = AirQualityData.fromDictionary(objectDictionary: json) else { completion(nil); return }
            
            📗("Parsed response data into model: \(aqData)")
            completion(aqData)
        })
    }
}
